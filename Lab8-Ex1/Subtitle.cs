﻿using System;
using System.Collections.Generic;

namespace Lab8_Ex1
{
    class Subtitle
    {
        public int StartTime, FinishTime;
        public string Text;
        public static int MaxTime;
        public string Position { get; set; } = "Bottom";
        public ConsoleColor Color { get; set; } = Colors.GetValueOrDefault("White");

        public static readonly Dictionary<string, ConsoleColor> Colors = new()
        {
            { "White", ConsoleColor.White },
            { "Blue", ConsoleColor.Blue },
            { "Red", ConsoleColor.Red },
            { "Green", ConsoleColor.Green },
            { "Yellow", ConsoleColor.Yellow }
        };
    }
}
